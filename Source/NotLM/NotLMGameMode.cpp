// Copyright Epic Games, Inc. All Rights Reserved.

#include "NotLMGameMode.h"
#include "NotLMPlayerController.h"
#include "NotLMCharacter.h"
#include "UObject/ConstructorHelpers.h"

ANotLMGameMode::ANotLMGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ANotLMPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	//if (PlayerPawnBPClass.Class != nullptr)
	//{
	//	DefaultPawnClass = PlayerPawnBPClass.Class;
	//}
}
