// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogNotLM, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogNotLM_Net, Log, All);
