// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "WeaponDefault.h"
#include "NotLMPlayerController.h"
#include "NOTLMInventoryComponent.h"
#include "TPSCharacterHealthComponent.h"
#include "TPSHealthComponent.h"
#include "NOTLMStateEffect.h"
#include "NOTLMIGameActor.h"
#include "Net/UnrealNetwork.h"
#include "NotLMCharacter.generated.h"


UCLASS(Blueprintable)
class ANotLMCharacter : public ACharacter, public INOTLMIGameActor
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;
public:
	ANotLMCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
		class UNOTLMInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CharacterHealth", meta = (AllowPrivateAccess = "true"))
		class UTPSCharacterHealthComponent* CharacHealthComponent;
private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UDecalComponent* CursorToWorld;

public:
	FTimerHandle TimerHandle_RagDollTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", Replicated)
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", Replicated)
		bool AimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Death")
		bool bIsAlive = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Death")
		TArray<UAnimMontage*> DeadAnims;

	//ability
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HealAbility")
		TSubclassOf<UNOTLMStateEffect> HealAbilityEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LightingAbility")
		TSubclassOf<UNOTLMStateEffect> RingofLightAbilityEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LightingAbility")
		bool bIsRingOfLightCharged = true; //switch to false later!!!
	//

	UPROPERTY(Replicated)
	AWeaponDefault* CurrentWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	UDecalComponent* CurrentCursor = nullptr;

	TArray<UNOTLMStateEffect*> Effects;

	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();
	UFUNCTION()
		void MovementTick(float DeltaTime);
	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();// EMovementState NewMovementState);

	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake);
	UFUNCTION(BlueprintNativeEvent)
		void BP_WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void BP_WeaponReloadEnd(bool bIsSuccess);

	//functions delegates fire animation

	UFUNCTION()
		void WeaponFireAimStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponFireRunStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void BP_WeaponFireAimStart(UAnimMontage* Anim);

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;

	UFUNCTION(BlueprintCallable, Category = "LightingAbility")
		int32 GetCurrentIndexWeapon();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool GetIsAlive();

	void TrySwitchNextWeapon();
	void TrySwitchPreviousWeapon();
	void TryHealAbilityEnabled();
	void TryRingOfLightAbilityEnabled();

	//interface
	EPhysicalSurface GetSurfaceType() override;
	TArray<UNOTLMStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UNOTLMStateEffect* RemoveEffect) override;
	void AddEffect(UNOTLMStateEffect* NewEffect) override;


	UFUNCTION()
		void CharDead();
	void EnableRagdoll();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintNativeEvent)
		void BP_CharDead();

	UFUNCTION(Server, Unreliable)
		void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
		void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
		void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
		void SetMovementState_Multicast(EMovementState NewState);


};

