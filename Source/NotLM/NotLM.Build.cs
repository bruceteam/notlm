// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class NotLM : ModuleRules
{
	public NotLM(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore", "Slate" });
    }
}
