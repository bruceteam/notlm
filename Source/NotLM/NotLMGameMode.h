// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "NotLMGameMode.generated.h"

UCLASS(minimalapi)
class ANotLMGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ANotLMGameMode();
	
};



