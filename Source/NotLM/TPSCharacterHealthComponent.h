// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPSHealthComponent.h"
#include "TPSCharacterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class NOTLM_API UTPSCharacterHealthComponent : public UTPSHealthComponent
{
	GENERATED_BODY()

protected:
	float Shield = 100.0f;
public:
	UPROPERTY(BlueprintAssignable,EditAnywhere,BlueprintReadWrite, Category="Shield")
	FOnShieldChange OnShieldChange;
	
	FTimerHandle TimerHandle_CooldownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;
	
	void ChangeHealthValue(float ChangeValue) override;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category="Shield")
	float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category="Shield")
	float ShieldRecoveryValue = -1.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category="Shield")
	float ShieldRecoveryRate = 0.1f;

	float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);

	void CoolDownShieldEnd();

	void RecoveryShield();
	
	UFUNCTION(BlueprintCallable)
	float GetShieldValue();


};
