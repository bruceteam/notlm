// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "NOTLMIGameActor.h"
#include "NOTLMStateEffect.h"
#include "NOTLM_EnvironmentStructure.generated.h"

UCLASS()
class NOTLM_API ANOTLM_EnvironmentStructure : public AActor, public INOTLMIGameActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ANOTLM_EnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	EPhysicalSurface GetSurfaceType() override;

	TArray<UNOTLMStateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UNOTLMStateEffect* RemoveEffect) override;
	void AddEffect(UNOTLMStateEffect* NewEffect) override;


	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TArray<UNOTLMStateEffect*> Effects;

};
