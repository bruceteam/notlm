// Copyright Epic Games, Inc. All Rights Reserved.

#include "NotLM.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, NotLM, "NotLM" );

DEFINE_LOG_CATEGORY(LogNotLM)
DEFINE_LOG_CATEGORY(LogNotLM_Net)
 