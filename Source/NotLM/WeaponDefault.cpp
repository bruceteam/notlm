// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "NOTLMInventoryComponent.h"
#include "Net/UnrealNetwork.h"
//#include "NOTLMIGameActor.h"
#include "NOTLMStateEffect.h"


// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetReplicates(true);

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	DropShellLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("DropShellLocation"));
	DropShellLocation->SetupAttachment(RootComponent);

	DropClipLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("DropClipLocation"));
	DropClipLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}
void AWeaponDefault::FireTick(float DeltaTime)
{
	//	if (GetWeaponRound() > 0)
	//	{
	if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
	{
		if (FireTimer < 0.f)
		{
			if (!WeaponReloading)
			{
				Fire();
			}
		}
		else
		{
			FireTimer -= DeltaTime;
		}
	}
	// else
	// {
	// 	if (!WeaponReloading && CheckCanWeaponReload())
	// 	{
	// 		InitReload();
	// 	}
	// 		
	// }
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer <= 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{

			CurrentDispersion = CurrentDispersionMin;

		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
	UpdateStateWeapon_OnServer(EMovementState::Run_State);
}

void AWeaponDefault::SetWeaponStateFire_OnServer_Implementation(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
	FireTimer = 0.01f;
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSetting.RateOfFire;
	AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round - 1;
	ChangeDispersionByShot();

	FXWeaponFire_Multicast(WeaponSetting.EffectFireWeapon, WeaponSetting.SoundFireWeapon);

	int8 NumberProjectile = GetNumberProjectileByShot();

	//delegate animation Which stand we are

	if (bAimFlag)
	{
		if (WeaponSetting.AnimCharFireAim)
		{
			OnWeaponFireAimStart.Broadcast(WeaponSetting.AnimCharFireAim);
		}
	}
	else
	{
		if (WeaponSetting.AnimCharFireRun)
		{
			OnWeaponFireAimStart.Broadcast(WeaponSetting.AnimCharFireRun);
		}
	}

	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;

		for (int8 i = 0; i < NumberProjectile; i++)//Shotgun
		{
			EndLocation = GetFireEndLocation();

			if (ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic fire

				FVector Dir = EndLocation - SpawnLocation;

				Dir.Normalize();

				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();


				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();


				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
				//	UE_LOG(LogTemp, Warning, TEXT("i = %i"), i);
			}
			else
			{
				//ToDo Projectile null Init trace fire			
				FHitResult Hit;
				TArray<AActor*> Actors;

				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSetting.DistanceTrace,
					ETraceTypeQuery::TraceTypeQuery2, false, Actors, EDrawDebugTrace::ForDuration,
					Hit, true, FColor::Red, FLinearColor::Green, 5.0f);

				if (ShowDebug)
				{
					DebugTraceWeaponFire_Multicast(GetWorld(), SpawnLocation, WeaponSetting.DistanceTrace, ShowDebug, ShootLocation);
				}


				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfacetype))
					{
						UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfacetype];
						if (myMaterial && Hit.GetComponent())
						{
							//UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None,
							//	Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition);
							WeaponSpawnHitDecal_Multicast(myMaterial, Hit);
						}
					}

					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfacetype))
					{
						UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurfacetype];
						if (myParticle)
						{
							//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(),
							//	Hit.ImpactPoint, FVector(1.0f)));
							WeaponSpawnHitFX_Multicast(GetWorld(),myParticle, Hit);
						}
					}

					if (WeaponSetting.ProjectileSetting.HitSound)
					{
						//UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.HitSound, Hit.ImpactPoint);
						WeaponSpawnHitSound_Multicast(GetWorld(),WeaponSetting.ProjectileSetting.HitSound, Hit);
					}

					UTypes::AddEffectBySurfaceType(Hit.GetActor(), Hit.BoneName, ProjectileInfo.Effect, mySurfacetype);



					//if (Hit.GetActor()->GetClass()->ImplementsInterface(UNOTLMIGameActor::StaticClass()))
					//{
						//INOTLMIGameActor::Execute_AvailableForEffects(Hit.GetActor());
						//INOTLMIGameActor::Execute_AvailableForEffectsBP(Hit.GetActor());

					//}


					//UNOTLMStateEffect* NewEffect = NewObject<UNOTLMStateEffect>(Hit.GetActor(), FName("Effect"));

					UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, NULL);
				}
				//GetWorld()->LineTraceSingleByChannel()
			}

		}

		if (WeaponSetting.ShellSettings.ShellBullets)
		{
			ShellDropFire_Multicast(GetWorld(), DropShellLocation, this, WeaponSetting.ShellSettings.ShellLifeTime, WeaponSetting.ShellSettings.ImpulseDrop, WeaponSetting.ShellSettings.ShellBullets);
			//FVector ShellLocation = DropShellLocation->GetComponentLocation();
			//FRotator ShellRotation = DropShellLocation->GetComponentRotation();
			//AStaticMeshActor* NewActor = nullptr;
			//FActorSpawnParameters params;
			//params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
			//params.Owner = this;
			//FVector multiplyImpulse = (DropShellLocation->GetComponentLocation())*WeaponSetting.ShellSettings.ImpulseDrop;
			//	
			//NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(ShellLocation,ShellRotation,params);
			//NewActor->SetLifeSpan(WeaponSetting.ShellSettings.ShellLifeTime);//InitialLifeSpan = WeaponSetting.ShellSettings.ShellLifeTime;
			//NewActor->GetStaticMeshComponent()->Mobility=EComponentMobility::Movable;
			//NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			//NewActor->GetStaticMeshComponent()->SetStaticMesh(WeaponSetting.ShellSettings.ShellBullets);
			//NewActor->GetStaticMeshComponent()->AddImpulse(multiplyImpulse);
		}
	}


	if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		if (CheckCanWeaponReload())
			InitReload();
	}
}


void AWeaponDefault::UpdateStateWeapon_OnServer_Implementation(EMovementState NewMovementState)
{

	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:

		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		bAimFlag = true;
		break;
	case EMovementState::Walk_State:

		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		bAimFlag = true;
		break;
	case EMovementState::Run_State:

		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		bAimFlag = false;
		break;
	case EMovementState::Sprint_State:
		BlockFire = true;
		SetWeaponStateFire_OnServer(false);//set fire trigger to false
		//Block Fire
		break;
	default:
		break;
	}
}



void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation),
				WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f,
				32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(),
				WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f,
				32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}


	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);


	}


	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	if (AdditionalWeaponInfo.Round < WeaponSetting.MaxRound)
	{
		if (!WeaponReloading)
		{
			WeaponReloading = true;
			ReloadTimer = WeaponSetting.ReloadTime;
			if (WeaponSetting.AnimCharReload)
			{
				OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReload);
			}

			if (WeaponSetting.ClipSettings.ClipDrop)
			{
				ClipDrop_Multicast(GetWorld(), DropClipLocation, this, WeaponSetting.ClipSettings.ClipLifeTime, WeaponSetting.ClipSettings.ImpulseDropClip, WeaponSetting.ClipSettings.ClipDrop, WeaponSetting.SoundReloadWeapon);
				/*		FVector ClipLocation = DropClipLocation->GetComponentLocation();
						FRotator ClipRotation = DropClipLocation->GetComponentRotation();
						AStaticMeshActor* NewActorr = nullptr;
						FActorSpawnParameters par;
						par.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
						par.Owner = this;
						FVector multiply = (DropClipLocation->GetComponentLocation())*WeaponSetting.ClipSettings.ImpulseDropClip;

						NewActorr = GetWorld()->SpawnActor<AStaticMeshActor>(ClipLocation,ClipRotation,par);
						NewActorr->SetLifeSpan(WeaponSetting.ClipSettings.ClipLifeTime);
						NewActorr->GetStaticMeshComponent()->Mobility=EComponentMobility::Movable;
						NewActorr->GetStaticMeshComponent()->SetSimulatePhysics(true);
						NewActorr->GetStaticMeshComponent()->SetStaticMesh(WeaponSetting.ClipSettings.ClipDrop);
						NewActorr->GetStaticMeshComponent()->AddImpulse(multiply);
						NewActorr->GetStaticMeshComponent()->AddWorldRotation(ClipRotation);*/
			}
		}
	}
}

//bug here or down here
void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;

	int8 AvailableAmmoFromInventory = GetAvailableAmmoForReload();
	int8 AmmoNeedToTakeFromInventory;
	int8 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round;

	if (NeedToReload > AvailableAmmoFromInventory)
	{
		AdditionalWeaponInfo.Round += AvailableAmmoFromInventory;
		AmmoNeedToTakeFromInventory = AvailableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.Round = AdditionalWeaponInfo.Round + NeedToReload;
		AmmoNeedToTakeFromInventory = NeedToReload;
	}

	//int32 AmmoNeedTake = AdditionalWeaponInfo.Round;
	//AmmoNeedTake = AmmoNeedTake - AvailableAmmoFromInventory;
	//AdditionalWeaponInfo.Round = AvailableAmmoFromInventory;
	//fix -/+ down here
	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedToTakeFromInventory);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	OnWeaponReloadEnd.Broadcast(false, 0);
}

int8 AWeaponDefault::GetAvailableAmmoForReload()
{
	int8 AvailableAmmoForWeapon = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		UNOTLMInventoryComponent* MyInventory = Cast<UNOTLMInventoryComponent>(GetOwner()->GetComponentByClass(UNOTLMInventoryComponent::StaticClass()));
		if (MyInventory)
		{
			if (MyInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon))
			{
				AvailableAmmoForWeapon = AvailableAmmoForWeapon;
			}
		}
	}
	return AvailableAmmoForWeapon;
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;

	if (GetOwner())
	{
		UNOTLMInventoryComponent* MyInventory = Cast<UNOTLMInventoryComponent>(GetOwner()->GetComponentByClass(UNOTLMInventoryComponent::StaticClass()));
		if (MyInventory)
		{
			int8 AvailableAmmoForWeapon;
			if (!MyInventory->CheckAmmoForWeapon(WeaponSetting.WeaponType, AvailableAmmoForWeapon))
			{
				result = false;
			}
		}
	}

	return result;
}

void AWeaponDefault::DebugTraceWeaponFire_Multicast_Implementation(UWorld* World, FVector Loc, float Distance, bool Debug, UArrowComponent* ShootLoc)
{
	if (Debug)
	{
		DrawDebugLine(GetWorld(), Loc, Loc + ShootLoc->GetForwardVector() * Distance,
			FColor::Red, false, 5.0f, (uint8)'\000', 1.5f);
	}
}

void AWeaponDefault::FXWeaponFire_Multicast_Implementation(UParticleSystem* FXFire, USoundBase* SoundFire)
{
	if (SoundFire)
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SoundFire, ShootLocation->GetComponentLocation());
	if (FXFire)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FXFire, ShootLocation->GetComponentTransform());
}

void AWeaponDefault::ShellDropFire_Multicast_Implementation(UWorld* World, UArrowComponent* DropLocation, AWeaponDefault* WeaponActor, float LifeTime, float ImpulseDrop, UStaticMesh* ShellBullets)
{
	FVector ShellLocation = DropLocation->GetComponentLocation();
	FRotator ShellRotation = DropLocation->GetComponentRotation();
	AStaticMeshActor* NewActor = nullptr;
	FActorSpawnParameters params;
	params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	params.Owner = WeaponActor;
	FVector multiplyImpulse = (DropLocation->GetComponentLocation()) * ImpulseDrop;

	NewActor = World->SpawnActor<AStaticMeshActor>(ShellLocation, ShellRotation, params);
	NewActor->SetLifeSpan(LifeTime);//InitialLifeSpan = WeaponSetting.ShellSettings.ShellLifeTime;
	NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
	NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
	NewActor->GetStaticMeshComponent()->SetStaticMesh(ShellBullets);
	NewActor->GetStaticMeshComponent()->AddImpulse(multiplyImpulse);
}


void AWeaponDefault::ClipDrop_Multicast_Implementation(UWorld* World, UArrowComponent* DropLocation, AWeaponDefault* WeaponActor, float LifeTime, float ImpulseDrop, UStaticMesh* Clip, USoundBase* ReloadSound)
{
	if (ReloadSound)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ReloadSound, ShootLocation->GetComponentLocation());
	}

	FVector ClipLocation = DropLocation->GetComponentLocation();
	FRotator ClipRotation = DropLocation->GetComponentRotation();
	AStaticMeshActor* NewActorr = nullptr;
	FActorSpawnParameters par;
	par.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	par.Owner = WeaponActor;
	FVector multiply = (DropLocation->GetComponentLocation()) * ImpulseDrop;

	NewActorr = World->SpawnActor<AStaticMeshActor>(ClipLocation, ClipRotation, par);
	NewActorr->SetLifeSpan(LifeTime);
	NewActorr->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
	NewActorr->GetStaticMeshComponent()->SetSimulatePhysics(true);
	NewActorr->GetStaticMeshComponent()->SetStaticMesh(Clip);
	NewActorr->GetStaticMeshComponent()->AddImpulse(multiply);
	NewActorr->GetStaticMeshComponent()->AddWorldRotation(ClipRotation);
}

void AWeaponDefault::UpdateWeaponByCharacterMovementState_OnServer_Implementation(FVector NewShootEndLocation, bool NewShouldReduceDispersion)
{
	ShootEndLocation = NewShootEndLocation;
	ShouldReduceDispersion = NewShouldReduceDispersion;
}

void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeaponDefault, AdditionalWeaponInfo);

}

void AWeaponDefault::WeaponSpawnHitDecal_Multicast_Implementation(UMaterialInterface* DecalMaterial, FHitResult HitResult)
{
	UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(20.0f), HitResult.GetComponent(), NAME_None,
								HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition);
}

void AWeaponDefault::WeaponSpawnHitFX_Multicast_Implementation(UWorld* World,UParticleSystem* FXTemplate, FHitResult HitResult)
{
	UGameplayStatics::SpawnEmitterAtLocation(World, FXTemplate, FTransform(HitResult.ImpactNormal.Rotation(),
								HitResult.ImpactPoint, FVector(1.0f)));
}

void AWeaponDefault::WeaponSpawnHitSound_Multicast_Implementation(UWorld* World,USoundBase* HitSound, FHitResult HitResult)
{
	UGameplayStatics::PlaySoundAtLocation(World, HitSound, HitResult.ImpactPoint);
}
