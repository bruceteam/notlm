// Fill out your copyright notice in the Description page of Project Settings.


#include "NOTLMStateEffect.h"
#include "Kismet/GameplayStatics.h"
#include "NOTLMIGameActor.h"
#include "NotLMCharacter.h"
#include "TPSHealthComponent.h"

bool UNOTLMStateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;

	INOTLMIGameActor* myInterface = Cast<INOTLMIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}


void UNOTLMStateEffect::DestroyObject()
{
	INOTLMIGameActor* myInterface = Cast<INOTLMIGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;

	if (this && this->IsValidLowLevel())
	{

		this->ConditionalBeginDestroy();
	}
}

bool UNOTLM_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();
	return true;
}

void UNOTLM_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UNOTLM_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));

		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}

	}

	DestroyObject();
}

bool UNOTLM_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{


	Super::InitObject(Actor, NameBoneHit);


	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UNOTLM_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UNOTLM_StateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{
		FName NameBonetoAttached = NameBoneHit;
		FVector Location = FVector(0);

		USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));

		if (myMesh)
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myMesh, NameBonetoAttached, Location,
				FRotator::ZeroRotator, EAttachLocation::SnapToTarget);
		}
		else
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBonetoAttached, Location,
				FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
			//UE_LOG(LogTemp, Display, TEXT("Shit"));
		}


	}
	return true;
}

void UNOTLM_StateEffect_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	//	Super::DestroyObject();
	//GetWorld()->GetTimerManager().ClearTimer(TimerHandle_EffectTimer);
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ExecuteTimer);
	UNOTLMStateEffect::DestroyObject();
}

void UNOTLM_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));

		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}

bool UNOTLM_StateEffect_DamageArea::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UNOTLM_StateEffect_DamageArea::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UNOTLM_StateEffect_DamageArea::Execute, RateTime, true);

	if (ParticleEffect)
	{
		FName NameBonetoAttached;
		FVector Location = FVector(0);

		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBonetoAttached, Location,
			FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);

	}
	return true;
}

void UNOTLM_StateEffect_DamageArea::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	//Super::DestroyObject();
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ExecuteTimer);
	UNOTLMStateEffect::DestroyObject();
}

void UNOTLM_StateEffect_DamageArea::Execute()
{
	if (myActor)
	{
		TArray<AActor*> IgnoredActor;
		FVector Loc = myActor->GetActorLocation();
		APawn* myChar = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);

		if (myChar)
		{
			UGameplayStatics::ApplyRadialDamage(GetWorld(),
				Power,
				Loc,
				Radius,
				UDamageType::StaticClass(),
				IgnoredActor,
				myChar,
				nullptr,
				true,
				ECollisionChannel::ECC_Visibility
			);

			if (SoundEffect)
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), SoundEffect, Loc);
			}

		}
	}
}