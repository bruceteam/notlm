// Fill out your copyright notice in the Description page of Project Settings.


#include "NOTLM_EnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
ANOTLM_EnvironmentStructure::ANOTLM_EnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANOTLM_EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANOTLM_EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ANOTLM_EnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0); 
		
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	
	
	return Result;
}

TArray<UNOTLMStateEffect*> ANOTLM_EnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ANOTLM_EnvironmentStructure::RemoveEffect(UNOTLMStateEffect* RemoveEffect)
{
	//RemoveEffect->BeginDestroy();
	Effects.Remove(RemoveEffect);
}

void ANOTLM_EnvironmentStructure::AddEffect(UNOTLMStateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}



