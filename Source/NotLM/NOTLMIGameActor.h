// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "NOTLMStateEffect.h"
#include "NOTLMIGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UNOTLMIGameActor : public UInterface
{
	GENERATED_BODY()
};


class NOTLM_API INOTLMIGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	//UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Event")
	//void AvailableForEffectsBP();

	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="Event")
	//bool AvailableForEffects();

	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UNOTLMStateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UNOTLMStateEffect* RemoveEffect);
	virtual void AddEffect(UNOTLMStateEffect* NewEffect);



};
