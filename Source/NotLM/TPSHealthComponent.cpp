// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSHealthComponent.h"


UTPSHealthComponent::UTPSHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UTPSHealthComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UTPSHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

float UTPSHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTPSHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UTPSHealthComponent::ChangeHealthValue(float ChangeValue)
{
	ChangeValue *= CoefDamage;
	Health -= ChangeValue;
	OnHealthChange.Broadcast(Health, ChangeValue);

	if(Health >= MaxHealth)
	{
		Health = MaxHealth;
	}
	else
	{
		if (Health <= 0.0f)
		{
			OnDead.Broadcast();
		}
	}
}


