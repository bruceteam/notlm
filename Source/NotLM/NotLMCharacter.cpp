// Copyright Epic Games, Inc. All Rights Reserved.

#include "NotLMCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Engine/World.h"
#include "MyGameInstance.h"
#include "NotLM.h"
#include "Net/UnrealNetwork.h"
#include "ProjectileDefault.h"

DEFINE_LOG_CATEGORY_STATIC(Characterlog, All, All);

ANotLMCharacter::ANotLMCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UNOTLMInventoryComponent>(TEXT("InventoryComponent"));
	CharacHealthComponent = CreateDefaultSubobject<UTPSCharacterHealthComponent>(TEXT("CharacterHealthComponent"));

	if (CharacHealthComponent)
	{
		CharacHealthComponent->OnDead.AddDynamic(this, &ANotLMCharacter::CharDead);
	}

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ANotLMCharacter::InitWeapon);
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//net
	bReplicates = true;
}

void ANotLMCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC && myPC->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ANotLMCharacter::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName);

	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		if (CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)
		{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}
}

void ANotLMCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);
	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ANotLMCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ANotLMCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ANotLMCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ANotLMCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ANotLMCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("NextWeapon"), EInputEvent::IE_Pressed, this, &ANotLMCharacter::TrySwitchNextWeapon);
	NewInputComponent->BindAction(TEXT("PreviousWeapon"), EInputEvent::IE_Pressed, this, &ANotLMCharacter::TrySwitchPreviousWeapon);

	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ANotLMCharacter::TryHealAbilityEnabled);
	NewInputComponent->BindAction(TEXT("RingOfLightAbility"), EInputEvent::IE_Pressed, this, &ANotLMCharacter::TryRingOfLightAbilityEnabled);

}

void ANotLMCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ANotLMCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}


void ANotLMCharacter::MovementTick(float DeltaTime)
{
	if (bIsAlive)
	{

		if (GetController() && GetController()->IsLocalPlayerController())
		{

			AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
			AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

			FString SEnum = UEnum::GetValueAsString(EMovementState());
			UE_LOG(LogNotLM_Net, Warning, TEXT("Movement state -  %s"), *SEnum);

			if (MovementState == EMovementState::Sprint_State)
			{
				FVector MyRotationVector = FVector(AxisX, AxisY, 0.0f);
				FRotator myRotator = MyRotationVector.ToOrientationRotator();

				SetActorRotation(FQuat(myRotator));
				SetActorRotationByYaw_OnServer(myRotator.Yaw);
			}
			else
			{
				APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
				if (myController)
				{
					FHitResult ResultHit;

					myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
					float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
							SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));

					SetActorRotationByYaw_OnServer(FindRotatorResultYaw);

					if (CurrentWeapon)
					{
						FVector Displacement = FVector(0);
						bool bIsReduceDispersion = false;
						switch (MovementState)
						{
						case EMovementState::Aim_State:
							Displacement = FVector(0.0f, 0.0f, 160.0f);
							//CurrentWeapon->ShouldReduceDispersion = true;
							bIsReduceDispersion = true;
							break;
						case EMovementState::Walk_State:
							Displacement = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouldReduceDispersion = true;
							break;
						case EMovementState::Run_State:
							Displacement = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouldReduceDispersion = false;
						
							break;
						case EMovementState::Sprint_State:
							break;
						default:
							break;
						}


						//CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
						CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(ResultHit.Location + Displacement, bIsReduceDispersion);
					}
				}
			}
		}

	}
}

void ANotLMCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ANotLMCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void ANotLMCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;

}
void ANotLMCharacter::InputAttackPressed()
{

	if (bIsAlive)
	{
		AttackCharEvent(true);
	}
}

void ANotLMCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ANotLMCharacter::ChangeMovementState()//EMovementState NewMovementState)
{
	//if(!WalkEnabled)
	//MovementState = NewMovementState;
	EMovementState NewState = EMovementState::Run_State;

	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		NewState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			NewState = EMovementState::Sprint_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && AimEnabled)
			{
				NewState = EMovementState::Aim_State;
			}
			else
			{
				if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
				{
					NewState = EMovementState::Walk_State;
				}
				else
				{
					if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
					{
						NewState = EMovementState::Aim_State;
					}
				}
			}

		}
	}
	SetMovementState_OnServer(NewState);

	//CharacterUpdate();

	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon_OnServer(NewState);
	}
}

AWeaponDefault* ANotLMCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ANotLMCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	//server now
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UMyGameInstance* myGI = Cast<UMyGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{

			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;
					myWeapon->WeaponSetting = myWeaponInfo;
					//fix	myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound;
					//	myWeapon->ReloadTimer = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon_OnServer(MovementState);

					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;

					CurrentIndexWeapon = NewCurrentIndexWeapon;//InventoryComponent->GetWeaponIndexSlotByName(IdWeaponName);


					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ANotLMCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ANotLMCharacter::WeaponReloadEnd);
					myWeapon->OnWeaponFireAimStart.AddDynamic(this, &ANotLMCharacter::WeaponFireAimStart);
					myWeapon->OnWeaponFireRunStart.AddDynamic(this, &ANotLMCharacter::WeaponFireRunStart);

					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void ANotLMCharacter::TryReloadWeapon()
{
	if (bIsAlive && CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		if (CurrentWeapon->GetWeaponRound() <= CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
		{
			CurrentWeapon->InitReload();
		}
	}
}

UDecalComponent* ANotLMCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}
void ANotLMCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	BP_WeaponReloadStart(Anim);
}
void ANotLMCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	BP_WeaponReloadEnd(bIsSuccess);
}
void ANotLMCharacter::BP_WeaponReloadStart_Implementation(UAnimMontage* Anim)
{
	//in BP
}
void ANotLMCharacter::BP_WeaponReloadEnd_Implementation(bool bIsSuccess)
{
	//in BP
}

void ANotLMCharacter::WeaponFireAimStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	BP_WeaponFireAimStart(Anim);
}

void ANotLMCharacter::WeaponFireRunStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	BP_WeaponFireAimStart(Anim);
}


void ANotLMCharacter::BP_WeaponFireAimStart_Implementation(UAnimMontage* Anim)
{
	//in BP
}

bool ANotLMCharacter::GetIsAlive()
{
	return bIsAlive;
}

void ANotLMCharacter::TrySwitchNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		UE_LOG(LogTemp, Warning, TEXT("U just tryed to switch to the next weapon!!!!"));
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{

			}
		}
	}
}

void ANotLMCharacter::TrySwitchPreviousWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		UE_LOG(LogTemp, Warning, TEXT("U just tryed to switch to the previous weapon!!!!"));
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{

			}
		}
	}
}

int32 ANotLMCharacter::GetCurrentIndexWeapon()
{
	return CurrentIndexWeapon;
}

void ANotLMCharacter::TryHealAbilityEnabled()
{
	if (HealAbilityEffect)
	{
		UNOTLMStateEffect* NewEffect = NewObject<UNOTLMStateEffect>(this, HealAbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}

void ANotLMCharacter::TryRingOfLightAbilityEnabled()
{
	if (bIsRingOfLightCharged)
	{
		if (RingofLightAbilityEffect)
		{
			//UE_LOG(LogTemp, Display, TEXT("Ring Is Charged!"));
			UNOTLMStateEffect* NewEffect = NewObject<UNOTLMStateEffect>(this, RingofLightAbilityEffect);
			if (NewEffect)
			{
				bIsRingOfLightCharged = false;
				NewEffect->InitObject(this, NAME_None);
			}
		}
	}

}

void ANotLMCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int32 Rnd = FMath::RandHelper(DeadAnims.Num());
	if (DeadAnims.IsValidIndex(Rnd) && DeadAnims[Rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadAnims[Rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadAnims[Rnd]);
	}
	bIsAlive = false;

	if (GetController())
	{
		GetController()->UnPossess();
	}
	UnPossessed();

	//timer ragdoll
	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ANotLMCharacter::EnableRagdoll, TimeAnim, false);
	GetCursorToWorld()->SetVisibility(false);

	AttackCharEvent(false);

	BP_CharDead();
}

void ANotLMCharacter::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}




float ANotLMCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		CharacHealthComponent->ChangeHealthValue(DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfaceType());

		}
	}


	return ActualDamage;
}

void ANotLMCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void ANotLMCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ANotLMCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
}

void ANotLMCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}

void ANotLMCharacter::BP_CharDead_Implementation()
{
	//BP
}


EPhysicalSurface ANotLMCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;


	if (CharacHealthComponent)
	{
		if (CharacHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}

	return Result;

}

TArray<UNOTLMStateEffect*> ANotLMCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ANotLMCharacter::RemoveEffect(UNOTLMStateEffect* RemoveEffect)
{
	//RemoveEffect->BeginDestroy();
	Effects.Remove(RemoveEffect);
}

void ANotLMCharacter::AddEffect(UNOTLMStateEffect* NewEffect)
{
	Effects.Add(NewEffect);
}

void ANotLMCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ANotLMCharacter, MovementState);
	DOREPLIFETIME(ANotLMCharacter, CurrentWeapon);
}