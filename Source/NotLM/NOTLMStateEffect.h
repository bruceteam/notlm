// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "NOTLMStateEffect.generated.h"


UCLASS(Blueprintable, BlueprintType)
class NOTLM_API UNOTLMStateEffect : public UObject
{
	GENERATED_BODY()

public:
	virtual bool InitObject(AActor* Actor, FName NameBoneHit);

	virtual void DestroyObject();



	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsStackable = false;


	AActor* myActor = nullptr;
};



UCLASS()
class NOTLM_API UNOTLM_StateEffect_ExecuteOnce : public UNOTLMStateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor,FName NameBoneHit) override;

	void DestroyObject() override;
	 
	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 20.0f;

};



UCLASS()
class NOTLM_API UNOTLM_StateEffect_ExecuteTimer : public UNOTLMStateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor,FName NameBoneHit) override;

	void DestroyObject() override;
	 
	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Timer")
		UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;
};

UCLASS()
class NOTLM_API UNOTLM_StateEffect_DamageArea : public UNOTLMStateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor,FName NameBoneHit) override;

	void DestroyObject() override;
	 
	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RingOfLight parameters")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RingOfLight parameters")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RingOfLight parameters")
		float RateTime = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "RingOfLight parameters")
		float Radius = 1000.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Effect")
		UParticleSystem* ParticleEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Effect")
		USoundBase* SoundEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;
};