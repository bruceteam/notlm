// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "NOTLM.h"
#include "NOTLMIgameActor.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor,FName NameBoneHit, TSubclassOf<UNOTLMStateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{

		UNOTLMStateEffect* myEffect = Cast<UNOTLMStateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;

			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;
					if (!myEffect->bIsStackable)
					{
						int8 j = 0;
						TArray<UNOTLMStateEffect*> CurrentEffects;
						INOTLMIGameActor* myInterface = Cast<INOTLMIGameActor>(TakeEffectActor);

						if (myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();
						}

						if (CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}

						}
						else
						{
							bIsCanAddEffect = true;
						}

					}
					else
					{
						bIsCanAddEffect = true;
					}


					if (bIsCanAddEffect)
					{
						//bIsCanAdd = true;
						UNOTLMStateEffect* NewEffect = NewObject<UNOTLMStateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor,NameBoneHit);
						}

					}

				}
				i++;

			}
		}

	}
}
