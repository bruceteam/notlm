// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();

}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);


}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();

		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!TimerEnabled)
	{
		Explose();
	}
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()
{
	if(bIsShowDebug)
	{
		DrawDebugSphere(GetWorld(),GetActorLocation(),ProjectileSetting.ProjectileMinRadiusDamage,20,FColor::Green,false,7.0f);
		DrawDebugSphere(GetWorld(),GetActorLocation(),ProjectileSetting.ProjectileMaxRadiusDamage,20,FColor::Red,false,7.0f);
	}
	
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		//UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
		SpawnExploseFX_Multicast(GetWorld(), GetActorLocation(), GetActorRotation(),ProjectileSetting.ExploseFX);
	}
	if (ProjectileSetting.ExploseSound)
	{
		//UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
		SpawnExploseSound_Multicast(GetWorld(),ProjectileSetting.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMinDamage,
		GetActorLocation(),
		ProjectileSetting.ProjectileMaxRadiusDamage,
		ProjectileSetting.ProjectileMinRadiusDamage,
		ProjectileSetting.DamageDegreesKoef,
		NULL, IgnoredActor, this, nullptr);


	this->Destroy();

}

void AProjectileDefault_Grenade::SpawnExploseFX_Multicast_Implementation(UWorld* World,FVector Loc, FRotator Rot, UParticleSystem* FXTemplate)
{
	UGameplayStatics::SpawnEmitterAtLocation(World, FXTemplate, Loc, Rot, FVector(1.0f));
}

void AProjectileDefault_Grenade::SpawnExploseSound_Multicast_Implementation(UWorld* World,USoundBase* HitSound, FVector Loc)
{
	UGameplayStatics::PlaySoundAtLocation(World, HitSound, Loc);
}
